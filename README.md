# Turtle Bot AI06

Python Project connecting to a turtle bot and using the sensors to move, targetting a green screen, based on ROS.

## Sensors

This project is made to use the camera (using OpenCV) and the LiDAR to follow the target and stop if something is detected nearby the robot to prevent it from colliding.

## Code

The functionnal code developed is available in [this folder](https://gitlab.com/elimouni/turtle-bot-ai06/-/tree/main/projet_workspace/src/projet_package). Use ROS to parameter and compile the project.
