#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/debian/Desktop/projet_workspace/devel:$CMAKE_PREFIX_PATH"
export PWD='/home/debian/Desktop/projet_workspace/build'
export ROSLISP_PACKAGE_DIRECTORIES='/home/debian/Desktop/projet_workspace/devel/share/common-lisp'
export ROS_PACKAGE_PATH="/home/debian/Desktop/projet_workspace/src:$ROS_PACKAGE_PATH"