#! /usr/bin/env python3

import rospy
import numpy as np
import cv2
from cv_bridge import CvBridge, CvBridgeError
from sensor_msgs.msg import Image
from geometry_msgs.msg import Point32
from geometry_msgs.msg import Twist

class MovementNode:
    def __init__(self):
        # Creates a node called and registers it to the ROS master
        rospy.init_node('movement_estimation_camera')

        # Camera variables
        self.camera_focal_distance = 3.04 * 0.001
        self.camera_width_angle = 62.2
        self.object_real_size = 64 * 0.001
        self.pixel_width = 1.12 * 0.000001
        self.camera_real_width = 3280

        # Publisher to the topic '/cmd_vel'.
        self.publisher = rospy.Publisher('/movement_estimation_camera', Point32, queue_size=10)

        # Subscriber to the input topic. self.callback is called when a message is received
        self.subscriber = rospy.Subscriber('~/center_image', Point32, self.callback)

    def callback(self, msg):
        '''
        Function called when an image is received.
        msg: Image message received
        '''
        # Convert ROS Image -> OpenCV
        image_data_x = msg.x
        image_data_width = msg.y
        image_data_width_tot = msg.z

        result_movement = Point32()

        # Calculating distance between object and robot
        distance_to_object = None
        if image_data_x != 0 :
            ratio_pixels = self.camera_real_width/image_data_width_tot
            distance_to_object = (self.camera_focal_distance * self.object_real_size /
                                (ratio_pixels * image_data_width * self.pixel_width))

        # Choosing linear motion
        threshold_linear = 0.04
        distance_limit = 0.30

        if distance_to_object != None and (distance_to_object - threshold_linear > distance_limit):
            result_linear_movement = distance_to_object / 2
            result_linear_movement = 1 if result_linear_movement > 1 else result_linear_movement
            result_movement.x = result_linear_movement
        elif distance_to_object != None and (distance_to_object + threshold_linear < distance_limit):
            if distance_to_object + threshold_linear < distance_limit * 0.80:
                result_movement.x =-0.6
            else:
                result_movement.x =-0.3
        else:
            result_movement.x=0
        
        # Calculating angular motion
        inf_angular_speed = -1
        sup_angular_speed = 1
        b = 0 + inf_angular_speed
        a = (sup_angular_speed - inf_angular_speed) * sup_angular_speed / (image_data_width_tot + b)
        correction_direction = -1
        
        angular_speed = (a * image_data_x + b) * correction_direction

        if (image_data_x > image_data_width_tot/1.78 or image_data_x < image_data_width_tot/2.28) and image_data_x != 0:
            result_movement.z = angular_speed
            result_movement.x = 0
        else:
            result_movement.z=0
        
        self.publisher.publish(result_movement)

if __name__ == '__main__':
    # Start the node and wait until it receives a message or stopped by Ctrl+C
    node = MovementNode()
    rospy.spin()
