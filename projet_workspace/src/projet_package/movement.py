#! /usr/bin/env python3

import rospy
import numpy as np
import cv2
from cv_bridge import CvBridge, CvBridgeError
from sensor_msgs.msg import Image
from geometry_msgs.msg import Point32
from geometry_msgs.msg import Twist

class MovementNode:
    def __init__(self):
        # Creates a node called and registers it to the ROS master
        rospy.init_node('movement')
        self.detected_lidar = True

        # Publisher to the topic '/cmd_vel'.
        self.publisher = rospy.Publisher('/cmd_vel', Twist, queue_size=10)

        # Subscriber to the input topic. self.callback is called when a message is received
        self.subscriberCamera = rospy.Subscriber('/movement_estimation_camera', Point32, self.callback)
        self.subscriberLidar = rospy.Subscriber('/movement_lidar', Point32, self.callbackLidar)

    def callback(self, msg):
        '''
        Function called when an image is received.
        msg: Image message received
        '''
        # Getting data of the camera
        camera_linear = msg.x
        camera_angular = msg.z

        result_movement = Twist()

        if not self.detected_lidar:
            result_movement.linear.x = camera_linear
            result_movement.angular.z = camera_angular
        else:
            result_movement.linear.x = 0
            result_movement.angular.z = 0

        print(result_movement.linear.x)
        print(result_movement.angular.z)
        
        self.publisher.publish(result_movement)

    def callbackLidar(self, msg):
        status_lidar = msg.x
        self.detected_lidar = True if status_lidar else False

if __name__ == '__main__':
    # Start the node and wait until it receives a message or stopped by Ctrl+C
    node = MovementNode()
    rospy.spin()
