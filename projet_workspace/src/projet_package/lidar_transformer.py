#!/usr/bin/env python3

import numpy as np
import math
import rospy

# Type of input and output messages
from sensor_msgs.point_cloud2 import create_cloud
from sensor_msgs.msg import LaserScan, PointCloud2, PointField
from geometry_msgs.msg import Point32

PC2FIELDS = [PointField('x', 0, PointField.FLOAT32, 1),
             PointField('y', 4, PointField.FLOAT32, 1),
             PointField('z', 8, PointField.FLOAT32, 1),
             PointField('c', 12, PointField.INT16, 1)
]

class LidarTransformer:
    def __init__(self):
        rospy.init_node('lidar_transformer')

        self.distance_limit = 0.20

        self.publisher = rospy.Publisher('/lidar/points', PointCloud2, queue_size=10)
        self.subscriber = rospy.Subscriber('/scan', LaserScan, self.callback)

    def callback(self, msg):
        coords = []

        for i, theta in enumerate(np.arange(msg.angle_min, msg.angle_max, msg.angle_increment)):
            # ToDo: Remove points too close
            if msg.ranges[i] <= 0.25 and msg.ranges[i] > 0.07:
                # ToDo: Polar to Cartesian transformation
                tupleCoor = [msg.ranges[i] * math.cos(theta), msg.ranges[i] * math.sin(theta)]
                coords.append(tupleCoor)
        # Create a PointCloud2 message from coordinates
        pc2 = create_cloud(msg.header, PC2FIELDS, [[x,y,0,0] for x,y in coords])
        self.publisher.publish(pc2)

if __name__ == '__main__':
    node = LidarTransformer()
    rospy.spin()
