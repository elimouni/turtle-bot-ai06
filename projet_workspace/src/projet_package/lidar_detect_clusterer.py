#!/usr/bin/env python3

import numpy as np
import math
import rospy

# Type of input and output messages
from sensor_msgs.point_cloud2 import create_cloud, read_points
from visualization_msgs.msg import MarkerArray, Marker
from sensor_msgs.msg import LaserScan, PointCloud2, PointField
from geometry_msgs.msg import Point32, Point

PC2FIELDS = [PointField('x', 0, PointField.FLOAT32, 1),
             PointField('y', 4, PointField.FLOAT32, 1),
             PointField('z', 8, PointField.FLOAT32, 1),
             PointField('c', 12, PointField.INT16, 1)
]

class LidarClusterer:
    def __init__(self):
        rospy.init_node('lidar_detect_clusterer')

        self.publisher = rospy.Publisher('/lidar/clusters', PointCloud2, queue_size=10)
        self.subscriber = rospy.Subscriber('/lidar/points', PointCloud2, self.callback)

    def callback(self, msg):
        points = np.array(list(read_points(msg)))
        groups = np.zeros(points.shape[0], dtype=int)

        # ToDo: Determine k and D values
        k = 10
        D = 0.5
        d = [0]*k

        # ToDo: Clustering algorithm
        for i in range(k, points.shape[0]):
            for j in range(1, k):
                d[j-1]=np.linalg.norm(points[i]-points[i-j])
            dmin = min(d[:-1])
            jmin = np.argmin(d[:-1])+1
            if dmin < D:
                if groups[i-jmin] == 0:
                    groups[i-jmin] = max(groups)+1
                groups[i] = groups[i-jmin]

        clust_msg = create_cloud(msg.header, PC2FIELDS, [[points[i,0],points[i,1],0,c] for i,c in enumerate(groups)])
        self.publisher.publish(clust_msg)

if __name__ == '__main__':
    node = LidarClusterer()
    rospy.spin()
