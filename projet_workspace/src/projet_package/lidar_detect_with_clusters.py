#!/usr/bin/env python3

import numpy as np
import math
import rospy

# Type of input and output messages
from sensor_msgs.point_cloud2 import create_cloud, read_points
from visualization_msgs.msg import MarkerArray, Marker
from sensor_msgs.msg import LaserScan, PointCloud2, PointField
from geometry_msgs.msg import Point32, Point

PC2FIELDS = [PointField('x', 0, PointField.FLOAT32, 1),
             PointField('y', 4, PointField.FLOAT32, 1),
             PointField('z', 8, PointField.FLOAT32, 1),
             PointField('c', 12, PointField.INT16, 1)
]

class LidarWithClusters:
    def __init__(self):
        rospy.init_node('lidar_detect_with_clusters')

        self.distance_limit = 0.20

        self.publisher = rospy.Publisher('/movement_lidar', Point32, queue_size=10)
        self.subscriber = rospy.Subscriber('/lidar/clusters', PointCloud2, self.callback)

    def callback(self, msg):
        clusters = {}
        for x,y,z,c in read_points(msg):
            if c not in clusters.keys(): clusters[c] = []
            clusters[c].append([x,y])

        result = Point32(None, None, None)

        for c, points in clusters.items():
            points = np.array(points)
            threshold = 2
            print(points)
            if not (len(points) < threshold):
                result = Point32(1,None,None)

        print(result)
        
        self.publisher.publish(result)

if __name__ == '__main__':
    node = LidarWithClusters()
    rospy.spin()
