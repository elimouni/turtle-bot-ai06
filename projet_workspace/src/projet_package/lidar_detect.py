#!/usr/bin/env python3

import numpy as np
import math
import rospy

# Type of input and output messages
from sensor_msgs.point_cloud2 import create_cloud
from sensor_msgs.msg import LaserScan, PointCloud2, PointField
from geometry_msgs.msg import Point32

PC2FIELDS = [PointField('x', 0, PointField.FLOAT32, 1),
             PointField('y', 4, PointField.FLOAT32, 1),
             PointField('z', 8, PointField.FLOAT32, 1),
             PointField('c', 12, PointField.INT16, 1)
]

class LidarDetect:
    def __init__(self):
        rospy.init_node('lidar_detect')

        self.distance_limit = 0.20

        self.publisher = rospy.Publisher('/movement_lidar', Point32, queue_size=10)
        self.subscriber = rospy.Subscriber('/scan', LaserScan, self.callback)

    def callback(self, msg):
        coords = []
        result = 0

        for i, theta in enumerate(np.arange(msg.angle_min, msg.angle_max, msg.angle_increment)):
            # ToDo: Remove points too close
            if msg.ranges[i] < self.distance_limit and msg.ranges[i]>0:
                result = 1
        
        self.publisher.publish(Point32(result, None, None))

if __name__ == '__main__':
    node = LidarDetect()
    rospy.spin()
