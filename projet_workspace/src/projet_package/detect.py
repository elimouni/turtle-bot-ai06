#! /usr/bin/env python3

import rospy
import numpy as np
import cv2
from cv_bridge import CvBridge, CvBridgeError
from sensor_msgs.msg import Image
from geometry_msgs.msg import Point32

class CameraNode:
    def __init__(self):
        # Creates a node called and registers it to the ROS master
        rospy.init_node('detect')

        # CvBridge is used to convert ROS messages to matrices manipulable by OpenCV
        self.bridge = CvBridge()

        # Initialize the node parameters
        #lower = np.array([0, 119, 0])
        #upper = np.array([100, 255, 100])
        upper = np.array([66, 255, 255])
        lower = np.array([40, 55, 50])
        self.interval = np.array([lower, upper])

        # Publisher to the output topics.
        # self.pub_img = rospy.Publisher('~output', Image, queue_size=10)
        self.pub_img_center = rospy.Publisher('~/center_image', Point32, queue_size=10)

        # Subscriber to the input topic. self.callback is called when a message is received
        self.subscriber = rospy.Subscriber('/camera/image_rect_color', Image, self.callback)

    def callback(self, msg):
        '''
        Function called when an image is received.
        msg: Image message received
        img_bgr: Width*Height*3 Numpy matrix storing the image
        '''
        # Convert ROS Image -> OpenCV
        try:
            img_bgr = self.bridge.imgmsg_to_cv2(msg, "bgr8")
            img_hsv = cv2.cvtColor(img_bgr, cv2.COLOR_BGR2HSV)
        except CvBridgeError as e:
            rospy.logwarn(e)
            return
        widthTot = msg.width
        pointX = None
        mask = cv2.inRange(img_hsv, self.interval[0], self.interval[1])
        _, contours, hierarchy = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        dispContours = cv2.drawContours(img_bgr, contours, -1, (0,255,0), 3)

        chosen = None
        maxArea = 0
        width = 0

        for c in contours:
            if maxArea < cv2.contourArea(c):
                chosen = c
                maxArea = cv2.contourArea(c)

        if chosen is not None:
            center = cv2.moments(chosen)
            cX = int(center["m10"] / center["m00"])
            cY = int(center["m01"] / center["m00"])

            minAll = np.amin(chosen, axis=0)[0]
            maxAll = np.amax(chosen, axis=0)[0]
            width = maxAll[0] - minAll[0]

            pointX = cX

            dispCenter = cv2.circle(img_bgr, (cX, cY), 5, (0,0,255), -1)
        else:
            dispCenter = img_bgr

        resultCenter = None

        if width >= 25:
            resultCenter = Point32(pointX, width, widthTot)
        else:
            resultCenter = Point32(None, None, widthTot)
        # Convert OpenCV -> ROS Image and publish
        try:
            # self.pub_img.publish(self.bridge.cv2_to_imgmsg(img_bgr, "bgr8")) # /!\ 'mono8' for grayscale images, 'bgr8' for color images
            # self.pub_img.publish(self.bridge.cv2_to_imgmsg(mask, "mono8"))
            # self.pub_img.publish(self.bridge.cv2_to_imgmsg(dispContours, "bgr8"))
            self.pub_img_center.publish(resultCenter)
        except CvBridgeError as e:
            rospy.logwarn(e)

if __name__ == '__main__':
    # Start the node and wait until it receives a message or stopped by Ctrl+C
    node = CameraNode()
    rospy.spin()
